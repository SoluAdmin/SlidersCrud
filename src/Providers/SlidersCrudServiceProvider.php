<?php

namespace SoluAdmin\SlidersCrud\Providers;

use SoluAdmin\Support\Providers\CrudServiceProvider;
use SoluAdmin\Support\Helpers\PublishableAssets as Assets;

class SlidersCrudServiceProvider extends CrudServiceProvider
{
    protected $assets = [
        Assets::CONFIGS,
        Assets::SEEDS,
        Assets::TRANSLATIONS,
        Assets::VIEWS,
        Assets::MIGRATIONS,
        Assets::MODULE_MIGRATIONS,
    ];

    protected $resources = [
        'slider' => 'SliderCrudController',
        'slider/{slider}/slides' => "SlidesCrudController",
    ];
}
