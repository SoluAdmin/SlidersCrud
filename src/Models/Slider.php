<?php

namespace SoluAdmin\SlidersCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $guarded = ['id'];

    protected $translatable = ['name'];

    protected $fillable = ['name'];

    public function slides()
    {
        return $this->hasMany(Slides::class)->orderBy('lft');
    }
    public function showSlidesButton()
    {

        $itemUrl = \Request::url() . '/' . $this->id . "/slides";
        $label = trans('SoluAdmin::SlidersCrud.see_items');

        return '
            <a href="'. $itemUrl .'" class="btn btn-xs btn-default">
            <i class="fa fa-eye"></i>
            '. $label .'
            </a>
        ';
    }
}
