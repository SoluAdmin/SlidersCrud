<?php


namespace SoluAdmin\SlidersCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Slides extends Model
{

    use CrudTrait;
    use HasTranslations;

    protected $fillable = ['name', 'caption','image','link', 'slider_id'];
    protected $translatable = ['name', 'caption','link','image'];
    protected $casts = [
        'routes' => 'array'
    ];

    public function slider()
    {
        $this->belongsTo(Slider::class);
    }
}
