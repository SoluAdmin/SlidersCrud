<?php


namespace SoluAdmin\SlidersCrud\Http\Controllers;

use SoluAdmin\SlidersCrud\Http\Requests\SlidesCrudRequest as StoreRequest;
use SoluAdmin\SlidersCrud\Http\Requests\SlidesCrudRequest as UpdateRequest;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class SlidesCrudController extends BaseCrudController
{

    public $sliderId;
    public $slideId;


    public function __construct()
    {
        parent::__construct();

        // Need to check for \Route::current() to avoid breaking route:list artisan command
        $this->sliderId = \Route::current()
            ? \Route::current()->parameter('slider')
            : null;

        $this->slideId = \Route::current()
            ? \Route::current()->parameter('slide')
            : null;
    }

    public function route()
    {
        return "slider/{$this->sliderId}/slides";
    }

    public function setUp()
    {
        parent::setup();
        $this->crud->addClause('where', 'slider_id', $this->sliderId);
        $this->crud->enableReorder('name', 1);
        $this->crud->allowAccess('reorder');
    }

    public function edit($id)
    {
        return parent::edit($this->slideId);
    }

    public function store(StoreRequest $request)
    {
        $request->request->add(['slider_id' => $this->sliderId]);
        return parent::storeCrud($request);
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud($request);
    }

    public function destroy($id)
    {
        return parent::destroy($this->slideId);
    }
}
