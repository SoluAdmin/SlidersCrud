<?php

namespace SoluAdmin\SlidersCrud\Http\Controllers;

use SoluAdmin\Support\Http\Controllers\BaseCrudController;
use SoluAdmin\SlidersCrud\Http\Requests\SliderCrudRequest as StoreRequest;
use SoluAdmin\SlidersCrud\Http\Requests\SliderCrudRequest as UpdateRequest;

class SliderCrudController extends BaseCrudController
{

    public function setup()
    {
        parent::setup();
        $this->crud->addButtonFromModelFunction('line', 'see_items', 'showSlidesButton', 'beginning');
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
