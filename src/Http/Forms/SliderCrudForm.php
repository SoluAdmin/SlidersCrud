<?php

namespace SoluAdmin\SlidersCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class SliderCrudForm implements Form
{
    public function fields()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::SlidersCrud.name'),
                'type' => 'text'
            ],
        ];
    }
}
