<?php


namespace SoluAdmin\SlidersCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class SlidesCrudForm implements Form
{

    public function fields()
    {

        return [
            [
                'label' => trans('SoluAdmin::SlidesCrud.name'),
                'name' => 'name',
                'type' => 'text'
            ],
            [
                'name' => 'caption',
                'label' => trans('SoluAdmin::SlidesCrud.caption'),
                'type' => 'text'
            ],
            [
                'name' => 'link',
                'label' => trans('SoluAdmin::SlidesCrud.link'),
                'type' => 'page_or_link',
                'page_model' => config('SoluAdmin.MenusCrud.page_model'),
            ],
            [
                'name' => 'image',
                'label' => trans('SoluAdmin::SlidesCrud.image'),
                'type' => 'browse',
            ],
            [
                'name' => 'status',
                'label' => trans('SoluAdmin::SlidesCrud.status'),
                'type' => 'radio',
                'options'     => [ // the key will be stored in the db, the value will be shown as label;
                    'PUBLISHED' => trans('SoluAdmin::SlidesCrud.active'),
                    'DRAFT' => trans('SoluAdmin::SlidesCrud.inactive'),
                ],
                "default" => 'PUBLISHED'
            ],

        ];
    }

    public function advancedFields()
    {
    }
}
