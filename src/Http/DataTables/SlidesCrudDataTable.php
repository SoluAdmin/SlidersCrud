<?php


namespace SoluAdmin\SlidersCrud\Http\DataTables;

use SoluAdmin\SlidersCrud\Models\Slides;
use SoluAdmin\Support\Interfaces\DataTable;

class SlidesCrudDataTable implements DataTable
{
    public function columns()
    {
        // TODO: Implement columns() method.
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::SlidesCrud.name')
            ],
            [
                'name' => 'caption',
                'label' => trans('SoluAdmin::SlidesCrud.caption'),
            ],
            [
                'name' => 'status',
                'label' => trans('SoluAdmin::SlidesCrud.status'),
                'type' => 'news_crud_enum',
            ],
        ];
    }
}
