<?php

namespace SoluAdmin\SlidersCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class SliderCrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::SlidersCrud.name'),
            ],

        ];
    }
}
