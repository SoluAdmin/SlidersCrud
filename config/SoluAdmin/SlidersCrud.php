<?php

return [
    'route_prefix' => '/sliders',
    'middleware' => false, // If we need to secure it we use middlewares, ex: ['can:do-anything']
    'setup_routes' => true,
    'publishes_migrations' => true,
    'page_model' => 'App\\Models\\Page',
];
