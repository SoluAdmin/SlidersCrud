@module("SoluAdmin\\SlidersCrud")

<li>
    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.SlidersCrud.route_prefix', '') . '/slider') }}">
        <i class="fa fa-image"></i> <span>{{trans('SoluAdmin::SlidersCrud.slider_plural')}}</span>
    </a>
</li>
@endmodule

