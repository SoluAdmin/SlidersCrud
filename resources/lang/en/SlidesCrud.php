<?php

return [
    'name' => 'Name',
    'caption' => 'Caption',
    'link' => 'Link',
    'image' => 'Image',
    'status' => 'Status',
    'active' => 'Active',
    'inactive' => 'Inactive',

];
