<?php

return [
    'slider_singular' => 'Slider',
    'slider_plural' => 'Sliders',
    'name' => 'Name',
    'see_items' => 'See Slides',
];
