<?php

return [
    'slider_singular' => 'Slider',
    'slider_plural' => 'Sliders',
    'see_items' => 'Ver Diapositivas',
    'slides_singular' => 'Slide',
    'slides_plural' => 'Slides',
];
