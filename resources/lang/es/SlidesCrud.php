<?php

return [
    'name' => 'Nombre',
    'caption' => 'Caption',
    'link' => 'Enlace',
    'image' => 'Imagen',
    'status' => 'Estado',
    'active' => 'Activo',
    'inactive' => 'Inactivo',

];
